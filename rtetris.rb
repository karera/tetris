class Tetris
    attr
      :block  # [color,[[y,x]]
      :board  # [[color]]
      :score
    def initialize(y,x)
      @block = rand_block
      @board = Array.new(y) { Array.new(x) { 0 } }
      @score = 0
    end


    Blocks = [
      [[0,0],[1,0],[2,0],[3,0]],
      [[0,0],[0,1],[0,2],[1,1]],
      [[0,0],[1,0],[2,0],[0,1]],
      [[0,0],[0,1],[1,1],[2,1]],
      [[0,0],[1,0],[1,1],[2,1]],
      [[0,1],[1,1],[1,0],[2,0]],
      [[0,0],[0,1],[1,0],[1,1]],
    ]

    def rand_block
      r = [
      [1, Blocks[0]],
      [2, Blocks[1]],
      [3, Blocks[2]],
      [4, Blocks[3]],
      [5, Blocks[4]],
      [6, Blocks[5]],
      [7, Blocks[6]],
      ]
      r.sample
    end

    def move?(bs)
      bs.all? do |y,x|
        @board.size > y && 0 <= y ?
          @board[y].size > x && 0 <= x ?
            @board[y][x] == 0 :
            false :
          false
      end
    end

    def game_is_over?(bs)
      if move?(bs)
        return false
      else
        bs.all? do |y,x|
          return true if 0 == y
          false
        end
      end
    end

    def rotate

        r = Math::PI / 2

        cy = (@block[1].map { |a| a[0] }.reduce(:+) / @block[1].size)
        cx = (@block[1].map { |a| a[1] }.reduce(:+) / @block[1].size)
        bs = @block[1].map do |y,x|
          [
            (cy + (x - cx) * Math.sin(r) + (y - cy) * Math.cos(r)).round,
            (cx + (x - cx) * Math.cos(r) - (y - cy) * Math.sin(r)).round
          ]
        end

        if move?(bs)
          @block[1] = bs
        end
      end

      # 下に動かした時
      def down
        bs = @block[1].map { |y,x| [y + 1, x] }
        if move?(bs)
          @block[1] = bs
          @score += 1
        end
      end

      # 右に動かした時
      def right
        bs = @block[1].map { |y,x| [y, x + 1] }
        if move?(bs)
          @block[1] = bs
        end
      end

      # 左に動かした時
      def left
        bs = @block[1].map { |y,x| [y, x - 1] }
        if move?(bs)
          @block[1] = bs
        end
      end

      # 時間で落ちる #Timersのgemをインストール
      require 'timers'
      def fall
        timers = Timers::Group.new
        bs = @block[1].map { |y,x| [y+1,x] }
        if move?(bs)
          @block[1] = bs
        else
          @block[1].each do |y,x|
            @board[y][x] = @block[0]
          end
          if game_is_over?(@block[1])
            timers.after(0.5) { sleep }
          end
          @block = rand_block
        end
      end

      # 列が揃ったら消去する
      def delete
        for y in 0 .. @board.size - 1
          if @board[y].all? { |c| c != 0 }
            @score += 100
            for yy in 0 .. y - 1
              @board[yy].each.with_index do |c,x|
                @board[y - yy][x] = @board[y - yy - 1][x]
              end
            end
          end
        end
      end

  require "curses"
  C = Curses

  def controller(c)

    case c
    when "w"
      rotate
    when C::KEY_DOWN
      down
    when C::KEY_RIGHT
      right
    when C::KEY_LEFT
      left
    when "q"
      C.close_screen
      exit
    else
      nil
    end
  end

  def display_init
    scr = C.init_screen
    C.noecho
    C.cbreak
    C.nonl
    C.start_color
    C.use_default_colors
    scr.keypad(true)
    C.curs_set(0)

    [
      C::COLOR_BLACK,
      C::COLOR_CYAN,
      C::COLOR_YELLOW,
      C::COLOR_GREEN,
      C::COLOR_RED,
      C::COLOR_BLUE,
      C::COLOR_WHITE,
      C::COLOR_MAGENTA,
    ].each.with_index do |c,i|
      C.init_pair(i, C::COLOR_WHITE, c)
    end
  end

  def display
    C.init_pair(11, C::COLOR_RED, C::COLOR_BLACK)
    C.clear
    C.addstr("Score:#{@score}\n")
    C.addstr("--" * (@board[0].size + 1))
    C.addstr("\n")
    for y in 0 .. @board.size - 1
      C.addstr("|")
      for x in 0 .. @board[y].size - 1
        c = @block[1].any? { |a| a == [y,x] } ? @block[0] : @board[y][x]
        C.attron(C.color_pair(c))
        C.addstr("  ")
        C.attroff(C.color_pair(c))
      end
      C.addstr("|")
      C.addstr("\n")
    end
    C.addstr("--" * (@board[0].size + 1))
    C.addstr("\n")

    if game_is_over?(@block[1])
      sleep 0.7
      C.clear
      C.addstr("Score:#{@score}\n")
      C.addstr("--" * (@board[0].size + 1))
      C.addstr("\n")
      for y in 0 .. 7
        C.addstr("|")
        for x in 0 .. @board[y].size - 1
          C.addstr("  ")
        end
        C.addstr("|")
        C.addstr("\n")
      end
      C.addstr("|")
      C.attron(C.color_pair(11))
      C.addstr("   -- Game Over --  ")
      C.attroff(C.color_pair(11))
      C.addstr("|")
      C.addstr("\n")
      C.addstr("|                    |")
      C.addstr("\n")
      C.addstr("|      Continue?     |")
      C.addstr("\n")
      C.addstr("|   Yes→y / No→n   |")
      C.addstr("\n")
      for y in 12 .. 19
        C.addstr("|")
        for x in 0 .. @board[y].size - 1
          C.addstr("  ")
        end
        C.addstr("|")
        C.addstr("\n")
      end
      C.addstr("--" * (@board[0].size + 1))

      n = C.getch.to_s
      if n == "y"
        Tetris.new(20,10).run
      elsif n == "n"
        C.close_screen
        exit
      end
    end
    C.refresh
  end

  def run
    display_init

    m = Mutex.new
    Thread.new do
      loop do
        m.synchronize do
          fall
          delete
          display
        end
        sleep 0.5
      end
    end

    loop do
      controller(C.getch())
      m.synchronize do
        delete
        display
      end
    end
  end
end


Tetris.new(20,10).run
