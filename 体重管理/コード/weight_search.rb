require "date"
require 'rdbi' 
require 'rdbi-driver-sqlite3' 
  
class WeightSearch
  def search 
    begin
      print "\n開始の日付を入力してください(yyyy-mm-dd)："
      $fromdate = gets.chomp.to_s
      if $fromdate == ""
        $fromdate = Date.new(1972-01-01)
      elsif 
        if /^\d{4}-\d{2}-\d{2}$/ !~ $fromdate
          raise
        else
          $fromdate = Date.strptime($fromdate,"%Y-%m-%d")
        end
      end
      print "\n終わりの日付を入力してください(yyyy-mm-dd)："
      $todate = gets.chomp.to_s
      if $todate == "" || nil
        $todate = Date.today
      else
        if /^\d{4}-\d{2}-\d{2}$/ !~ $todate
          raise  
        else
          $todate = Date.strptime($todate,"%Y-%m-%d")
        end
      end
        if $todate - $fromdate < 0
          raise
        end
        require './weight_curses.rb'
        graph_search = AllWeightInfos.new(20,35)
        graph_search.search_run
    rescue
      puts "正しい値を入力してください(yyyy-mm-dd)"
      search
    end
  end
end

