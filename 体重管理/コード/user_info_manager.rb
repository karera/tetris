require 'date'
require 'rubygems'
require 'rdbi'
require 'rdbi-driver-sqlite3'
require 'io/console'
require 'securerandom'
require './user_info.rb'
require './weight_info.rb'
require './weight_info_manager.rb'
require './constant.rb'


#UserInfoManagerクラスを定義
class UserInfoManager
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => "#{@db_name}" )
  end

  #処理の選択と選択後の処理を繰り返す
  def run
    loop do
      #機能選択画面の表示
      print Constant::USER_MENU

      #文字の入力を待つ
      num = gets.chomp
      case num
      when '1'
        #ユーザーの登録
        add_user_info
      when '2'
        #ユーザーの削除
        del_user_info
      when '3'
        #ユーザー選択
        select_user_info
      when '4'
        #ユーザー一覧
        show_user_info
      when '9'
        #データベースとの接続を終了
        @dbh.disconnect
        #アプリ終了
        puts "終了しました。"
        exit
      else
        puts "\n※正しい値を入力してください"
        #処理選択町画面に戻る
      end
    end
  end

private
  #ユーザーを登録する
  def add_user_info
    puts "\n1.ユーザーの登録"
    print "ユーザーを登録します。"

    #ユーザー1件分のインスタンスを作成する
    user_info = UserInfo.new( "", "" )
    #登録するデータを項目ごとに入力する
    print "\n"
    key = SecureRandom.random_number(n=100000)
    print "ユーザー名："
    user_info.name = gets.chomp
    print "パスワード："
    user_info.password = gets.chomp
    begin
      if user_info.name == "" || user_info.password == "" || /\s+/ =~ user_info.name || /\s+/ =~ user_info.password
        raise ArgumentError
      end
      if @dbh.execute("select count(*) from userinfos where name='#{user_info.name}'").fetch(:all)[0][0] == 1
        raise
      end

      #作成したユーザー1件分をデータベースに登録する
      @dbh.execute("insert into userinfos values (?, ?, ?);",
              key,
              user_info.name,
              user_info.password )
      puts "\n登録しました。"
      #体重のテーブルも作成する #テーブルは個人で一つ作る
      @dbh.execute("create table #{user_info.name}_weight(
        id             int(50)        not null,
        name           string(100)    not null,
        weight         int(10)        not null,
        date           string(100)    not null,
        primary        key(id));")
    rescue ArgumentError
      puts "\n※値を入力してください"
    rescue
      puts "\n※ユーザー名が重複しています"
    end
  end

  #ユーザーの削除
  def del_user_info
    begin
      puts Constant::USER_DELETE_MESSAGE
      #ユーザー名を指定してもらう
      print "ユーザー名を入力してください："
      name = gets.chomp
      if name == ""
        raise ArgumentError
      end

    #削除対象データを確認してから削除する
      if @dbh.execute("select count(*) from userinfos where name='#{name}'").fetch(:all)[0][0] == 1
        sth = @dbh.execute("select * from userinfos where name='#{name}'")
        sth.each do |row|
          puts "----------------"
          puts "ユーザー名：#{row[1]}"
          puts "----------------"
        end
        print "削除しますか？(パスワードを入力してください)："
        pass = STDIN.noecho(&:gets).chomp
        if pass == ""
          raise ArgumentError
        end
        if @dbh.execute("select count(*) from userinfos where  name='#{name}' and password='#{pass}'").fetch(:all)[0][0] == 1
          print "\n\n本当に削除しますか？(Y/yなら削除を実行します)："
          #読み込んだ値を大文字にそろえる
          yesno = gets.chomp.upcase
          #Yが1文字の時だけ削除を実行する
          if /^Y$/ =~ yesno
            #パスワードが合ったときだけ削除を実行する
            @dbh.execute("delete from userinfos where name='#{name}'")
            @dbh.execute("drop table #{name}_weight")
            puts "\nユーザーを削除しました"
          else
            puts "\n削除を中止しました"
          end
        else
          puts "\n※パスワードが間違っています"
        end
      else
        raise
      end
    rescue ArgumentError
      puts "\n※値を正しく入力してください"
    rescue
      puts "\nデータがありません"
    end
  end

#ユーザーを選択
  def select_user_info
    print "\n"
    print "ユーザー名とパスワードを入力してください\n"
    print "ユーザー名："
    name = gets.chomp
    print "パスワード："
    pass = STDIN.noecho(&:gets).chomp
    sth = @dbh.execute("select count(*) from userinfos where name='#{name}' and password='#{pass}'").fetch(:all)
    if sth[0][0] == 1
      puts "\n----------------"
      puts "ユーザー名：#{name}"
      puts "ログインしました"
      puts "----------------"
      $username = name
      weight_info_manager = WeightInfoManager.new(Constant::DB_NAME)
      weight_info_manager.run
    else
      puts "\n※ユーザー名またはパスワードが違います\n"
      user_info_manager = UserInfoManager.new(Constant::DB_NAME)
      user_info_manager.run
    end
  end

  #ユーザー一覧を表示する
  def show_user_info
    sth = @dbh.execute("select * from userinfos")
    puts "\n●ユーザー一覧●\n"
    sth.each do |row|
      puts "----------------"
      puts "ユーザー名：#{row[1]}"
    end
    puts "----------------"
    sth.finish
  end
end

#アプリケーションのインスタンスを作る
#ユーザーのSQLite3のデータベースを指定
user_info_manager = UserInfoManager.new(Constant::DB_NAME)

#Userの処理の選択と選択後の処理のくり返し
user_info_manager.run
