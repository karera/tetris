# 定数定義用モジュール
module Constant

  # ユーザーメニュー表示
  USER_MENU = "
  1.ユーザーの登録
  2.ユーザーの削除
  3.ユーザー選択
  4.ユーザー一覧
  9.終了\n
  番号を選んで下さい(1,2,3,4,9)：".freeze

  #体重のメニュー
  WEIGHT_MENU = "
  1.体重の入力
  2.体重の表示
  3.体重の修正
  4.ユーザー選択に戻る
  9.終了\n
  番号を選んで下さい(1,2,3,4,9)：".freeze

  #ユーザー削除メッセージ
  USER_DELETE_MESSAGE = "\nユーザーの削除\n\nユーザーを削除します。".freeze

  #削除メッセージ
  DELET_MESSAGE = "\n削除しますか？(Y/yなら削除を実行します)：".freeze

  #データベース名
  DB_NAME = "userinfo_sqlite.db".freeze

end
Constant.freeze
