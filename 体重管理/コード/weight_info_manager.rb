#WeightInfoManagerクラス
class WeightInfoManager
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => "#{@db_name}" )
  end

  #処理の選択と選択後の処理を繰り返す
  def run
    loop do
      #機能選択画面の表示
      print Constant::WEIGHT_MENU

      #文字の入力を待つ
      num = gets.chomp
      case num
      when '1'
        #体重の入力
        add_weight_info
      when '2'
        #体重の表示
        all_weight_infos
      when '3'
        #体重の削除
        del_weight_info
      when '4'
        #ユーザー選択に戻る
        user_info_manager = UserInfoManager.new(Constant::DB_NAME)
        user_info_manager.run
      when '9'
        #データベースとの接続を終了
        @dbh.disconnect
        #アプリ終了
        puts "終了しました。"
        exit
      else
        puts "\n※正しい値を入力してください"
        #処理選択町画面に戻る
      end
    end
  end

private
  #体重を登録する
  def add_weight_info
    puts "\n1.体重の登録"
    print "体重を登録します。"
    begin
      #体重1件分のインスタンスを作成する
      weight_info = WeightInfo.new( "", "", "" )
      #登録するデータを項目ごとに入力する
      print "\n体重："
      weight_info.weight = gets.chomp
      if /^\d{2,3}(\.\d)?$/ !~ weight_info.weight
        raise
      end

      key = SecureRandom.random_number(n=100000)
      weight_info.name = $username
      weight_info.date = Date.today.to_s
      sth = @dbh.execute("select count(*) from #{weight_info.name}_weight where date='#{weight_info.date}'").fetch(:all)
      if sth[0][0] == 1
        @dbh.execute("update #{weight_info.name}_weight set weight=#{weight_info.weight} where date='#{weight_info.date}';")
        puts "\n今日の体重を上書きしました。"
      else
        #作成した体重データ1件分をデータベースに登録する
        @dbh.execute("insert into #{weight_info.name}_weight values (?, ?, ?, ?);",
                key,
                weight_info.name,
                weight_info.weight,
                weight_info.date )
        puts "\n今日の体重を登録しました。"
      end
    rescue
      puts "\n体重は小数点第一位までの値を入力してください"
    end
  end

  #体重をグラフで表示する
  def all_weight_infos
    require './weight_curses.rb'
    require 'curses'
    all_weight_infos = AllWeightInfos.new(20,35)
    all_weight_infos.graph_run
  end

  #体重の修正
  def del_weight_info
    begin
      puts "\n3.体重の修正"
      print "体重を修正します。"
      print "\n"
      #日付を指定してもらう
      print "\n"
      print "日付を入力してください(yyyy-mm-dd)：\n"
      date = gets.chomp
      if /^\d{4}-\d{2}-\d{2}$/ !~ date
        raise
      end
      name = $username

      #対象データがあるか確認してから修正する
      if @dbh.execute("select count(*) from #{name}_weight where date='#{date}'").fetch(:all)[0][0] == 0
        raise
      end
      sth = @dbh.execute("select * from #{name}_weight where date='#{date}'")
      sth.each do |row|
        puts "----------------"
        puts "ユーザー名：#{row[1]}"
        puts "体重：#{row[2]}"
        puts "日付：#{row[3]}"
      end
      puts "----------------"

      puts "この日付の体重を修正します"
      puts "体重を入力してください："
      weight = gets.chomp
      if /^\d{2,3}(\.\d)?$/ !~ weight
        raise
      end
      @dbh.execute("update #{name}_weight set weight = #{weight} where date = '#{date}';")
      puts "\n体重を更新しました"
      sth = @dbh.execute("select * from #{name}_weight where date='#{date}'")
      sth.each do |row|
        puts "----------------"
        puts "ユーザー名：#{row[1]}"
        puts "体重：#{row[2]}"
        puts "日付：#{row[3]}"
      end
      puts "----------------"
    rescue
      puts "\n値を正しく入力してください"
    end
  end

end
