
#体重の値を管理するクラス
class WeightInfo
  #WeightInfoクラスのインスタンスを初期化
  def initialize ( name, weight, date )
    @name = name
    @weight = weight
    @date = date
  end

  attr_accessor :name, :weight, :date

  def to_s
    "#{@name},#{@weight},#{@date}"
  end

  def to_formatted_string( sep = "\n")
    "ユーザー名：#{@name}#{sep}体重：#{@pweight}kg#{sep}日付：#{@date}#{sep}"
  end
end
