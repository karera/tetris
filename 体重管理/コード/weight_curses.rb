require "date"
require 'rdbi' 
require 'rdbi-driver-sqlite3' 
require "curses"
require "./weight_info_manager.rb"


C = Curses

class AllWeightInfos 
  def initialize(y,x)
      @board = Array.new(y) { Array.new(x) { 0 } }
      @date = Date.today
        
  end
    
    # データベース読み込み、抽出
  def database_call
    dbh = RDBI.connect( :SQLite3, :database => 'userinfo_sqlite.db' )
    username = dbh.execute("select name from userinfos where name = '#{$username}'").fetch(:first)

    @db_call=[
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-10],1],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-9],2],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-8],3],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-7],4],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-6],6],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-5],1],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-4],2],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-3],3],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-2],4],
    [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[-1],6]
    ]
    @min =[
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-10],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-9],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-8],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-7],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-6],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-5],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-4],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-3],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-2],
      dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[-1]
    ].compact.min
    
=begin
    j=1
    for i in -10 .. -1 do
      if j == 5
        j = 1
      else j = j++
      @db_call = [dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all)[i],j].push    
      @min = dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)[i].push
      end
    end
    @min=@min.compact.min
=end
    dbh.disconnect
  end

    # スクリーン初期化
  def display_init
    C.init_screen
    C.noecho
    C.start_color
    C.use_default_colors
    C.curs_set(0)

    [
    C::COLOR_BLACK,
    C::COLOR_CYAN,
    C::COLOR_YELLOW,
    C::COLOR_GREEN,
    C::COLOR_RED,
    C::COLOR_BLUE,
    C::COLOR_WHITE,
    C::COLOR_MAGENTA,
    ].each.with_index do |c,i|
    C.init_pair(i, C::COLOR_BLACK, c)
    end
  end
    
  # スクリーン描画
  def display
    database_call
    C.clear
    
    @db_call.each{|(weight, date), col|
      C.addstr("          |\n")
      if date == nil
        C.addstr("          |")
      else
        C.addstr("#{date}|")
      end
      C.attron(C.color_pair(col))
      if weight == nil
        weight = 0
        C.addstr("\n")
      else 
        C.addstr(" " * (weight * 10 - @min[0].floor * 10 + 6))
        C.addstr("#{weight.to_f}\n")
      end
      C.attroff(C.color_pair(col))
     }
    C.addstr("          |\n          ")
    C.addstr("--" * (@board[0].size + 16))
    C.addstr("\n  ")
    if @min == nil
      C.addstr("        データがありません。")
    else
      for i in -1 .. 9
      C.addstr("        #{@min[0].floor + i}")
      end
    end
    C.addstr(" kg\n\n          表示を終わる：Qキー\n")
    C.addstr("          期間指定検索開始：Sキー")
    C.refresh
  end

  # 範囲指定検索
  def search_database
    dbh = RDBI.connect( :SQLite3, :database => 'userinfo_sqlite.db' )
    username = dbh.execute("select name from userinfos where name = '#{$username}'").fetch(:first)
    color = [1,2,3,4,5,6].cycle
    @db_search = dbh.execute("select weight, date from '#{username.join}_weight'").fetch(:all).zip(color)
    @search_min = dbh.execute("select weight from '#{username.join}_weight'").fetch(:all)
    @search_min = @search_min.compact.min
  end


  # 範囲指定検索グラフ描画
  def search_graph 
    search_database
    C.clear
      @db_search.each{|(weight, date), col|
        if date == nil
        else
          day = Date.strptime(date,"%Y-%m-%d").between?($fromdate,$todate)
          if day == true
            C.addstr("          |\n")
            C.addstr("#{date}")              # これはDate型
            C.addstr("|")
          else day == false
          end
        end
          C.attron(C.color_pair(col))
          if date == nil || day == false 
            weight = 0
          else 
            C.addstr(" " * (weight * 10 - @search_min[0].floor * 10 + 6))
            C.addstr("#{weight.to_f}\n")
          end
          C.attroff(C.color_pair(col))
      }
        C.addstr("          |\n          ")
        C.addstr("--" * (@board[0].size + 16))
        C.addstr("\n  ")
        if @search_min == nil
          C.addstr("        データがありません。")
        else
          for i in -1 .. 9
          C.addstr("        #{@search_min[0].floor + i}")
          end
        end
        C.addstr(" kg\n\n          戻る：Qキー\n")
        C.refresh
  end 



    
  # 全部動かすやつ
  def graph_run
    display_init
    loop do
      display
      n = C.getch.to_s
      if n == "q"
        C.close_screen
        weight_info_manager = WeightInfoManager.new("userinfo_sqlite.db")
        weight_info_manager.run
      elsif n == "s"
        C.close_screen
        require './weight_search.rb'
        weight_search = WeightSearch.new
        weight_search.search
      end
    end
  end

  def search_run
    C.close_screen
    display_init
    loop do
      search_graph
      n = C.getch.to_s
      if n == "q"
        C.close_screen
        weight_info_manager = WeightInfoManager.new("userinfo_sqlite.db")
        weight_info_manager.run
      else
      end
    end
  end

end






