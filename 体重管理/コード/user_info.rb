require 'date'
require 'rubygems'
require 'rdbi'
require 'rdbi-driver-sqlite3'

#ユーザーの値を管理するクラス
class UserInfo
  #UserInfoクラスのインスタンスを初期化
  def initialize ( name, password )
    @name = name
    @password = password
  end

  attr_accessor :name, :password

  def to_s
    "#{@name},#{@password}"
  end

  def to_formatted_string( sep = "\n")
    "ユーザー名：#{@name}#{sep}パスワード：#{@password}#{sep}"
  end
end
