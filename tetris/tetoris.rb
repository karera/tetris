class Tetris
    attr
      :block  # [color,[[y,x]]
      :board  # [[color]]
    def initialize(y,x)
      @block = rand_block
      @board = Array.new(y) { Array.new(x) { 0 } }
    end
    Blocks = [
      [[0,0],[1,0],[2,0],[3,0]],
      [[0,0],[0,1],[0,2],[1,1]],
      [[0,0],[1,0],[2,0],[0,1]],
      [[0,0],[0,1],[1,1],[2,1]],
      [[0,0],[1,0],[1,1],[2,1]],
      [[0,1],[1,1],[1,0],[2,0]],
      [[0,0],[0,1],[1,0],[1,1]],
    ]

    def rand_block
      [rand(1..7), Blocks[rand(Blocks.size)]]
    end

    #     def move?(bs)
        bs.all? do |y,x|
          @board.size > y && 0 <= y ?
            @board[y].size > x && 0 <= x ?
              @board[y][x] == 0 :
              false :
            false
        end
      end

      def rotate

        r = Math::PI / 2

        cy = (@block[1].map { |a| a[0] }.reduce(:+) / @block[1].size)
        cx = (@block[1].map { |a| a[1] }.reduce(:+) / @block[1].size)
        bs = @block[1].map do |y,x|
          [
            (cy + (x - cx) * Math.sin(r) + (y - cy) * Math.cos(r)).round,
            (cx + (x - cx) * Math.cos(r) - (y - cy) * Math.sin(r)).round
          ]
        end

        if move?(bs)
          @block[1] = bs
        end
      end

      # 下に動かした時
      def down
        bs = @block[1].map { |y,x| [y + 1, x] }
        if move?(bs)
          @block[1] = bs
        end
      end

      # 右に動かした時
      def right
        bs = @block[1].map { |y,x| [y, x + 1] }
        if move?(bs)
          @block[1] = bs
        end
      end

      # 左に動かした時
      def left
        bs = @block[1].map { |y,x| [y, x - 1] }
        if move?(bs)
          @block[1] = bs
        end
      end

      # 下に動かした時
      def fall
        bs = @block[1].map { |y,x| [y+1,x] }
        if move?(bs)
          @block[1] = bs
        else
          @block[1].each do |y,x|
            @board[y][x] = @block[0]
          end
          @block = rand_block
        end
      end

      # 列が揃ったら消去する
      def delete
        for y in 0 .. @board.size - 1
          if @board[y].all? { |c| c != 0 }
            for yy in 0 .. y - 1
              @board[yy].each.with_index do |c,x|
                @board[y - yy][x] = @board[y - yy - 1][x]
              end
            end
          end
        end
      end


      require "curses"
  C = Curses

  def controller(c)
    case c
    when "w"
      rotate
    when "s"
      down
    when "d"
      right
    when "a"
      left
    when "q"
      C.close_screen
      exit
    else
      nil
    end
  end

  def display_init
    C.init_screen
    C.start_color
    C.use_default_colors
    C.noecho
    C.curs_set(0)

    [
      C::COLOR_BLACK,
      C::COLOR_RED,
      C::COLOR_GREEN,
      C::COLOR_YELLOW,
      C::COLOR_BLUE,
      C::COLOR_MAGENTA,
      C::COLOR_CYAN,
      C::COLOR_WHITE,
    ].each.with_index do |c,i|
      C.init_pair(i, C::COLOR_WHITE, c)
    end
  end

  def display
    C.clear
    C.addstr("--" * (@board[0].size + 2))
    C.addstr("\n")
    for y in 0 .. @board.size - 1
      C.addstr("|")
      for x in 0 .. @board[y].size - 1
        c = @block[1].any? { |a| a == [y,x] } ? @block[0] : @board[y][x]
        C.attron(C.color_pair(c))
        C.addstr("  ")
        C.attroff(C.color_pair(c))
      end
      C.addstr("|")
      C.addstr("\n")
    end
    C.addstr("--" * (@board[0].size + 2))
    C.addstr("\n")
    C.refresh
  end



  def run
    display_init

    m = Mutex.new
    Thread.new do
      loop do
        m.synchronize do
          fall
          delete
          display
        end
        sleep 1
      end
    end

    loop do
      controller(C.getch.to_s)
      m.synchronize do
        delete
        display
      end
    end
  end
end


Tetris.new(20,20).run